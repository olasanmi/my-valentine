import { useState } from "react";
import "./App.css";

export default function Page() {
  const [noCount, setNoCount] = useState(0);
  const [mouseEnter, setMouseEnter] = useState(false)
  const [yesPressed, setYesPressed] = useState(false);
  const [noButtonPosition, setNoButtonPosition] = useState({ x: 0, y: 0 });
  const yesButtonSize = noCount * 20 + 16;

  const handleNoClick = () => {
    setNoCount(noCount + 1);
  };

  const getNoButtonText = () => {
    const phrases = [
      "No",
      "¿Estás segura?",
      "¿Realmente segura?",
      "¡Piénsalo de nuevo!",
      "¡Última oportunidad!",
      "¿Segura que no?",
      "¡Podrías arrepentirte de esto!",
      "¡Piénsalo otra vez!",
      "¿Estás absolutamente segura?",
      "¡Esto podría ser un error!",
      "¡Ten un corazón!",
      "¡No seas tan fría!",
      "¿Cambio de opinión?",
      "¿No reconsiderarías?",
      "¿Es esa tu respuesta final?",
      "¡Me estás rompiendo el corazón ;(",
    ];

    return phrases[Math.min(noCount, phrases.length - 1)];
  };

  const handleNoMouseMove = () => {
    setMouseEnter(true);
    const maxX = window.innerWidth - 100; // Ajusta el ancho máximo de la pantalla para evitar que el botón se salga del borde derecho
    const maxY = window.innerHeight - 40; // Ajusta el alto máximo de la pantalla para evitar que el botón se salga del borde inferior
    const randomX = Math.floor(Math.random() * maxX);
    const randomY = Math.floor(Math.random() * maxY);
    setNoButtonPosition({ x: randomX, y: randomY });
    handleNoClick()
    getNoButtonText()
  };

  return (
    <div
      className="centered-container"
    >
      <div className="valentine-container">
        {yesPressed ? (
          <>
            <div className="firework"></div>
            <div className="firework"></div>
            <div className="firework"></div>
            <img src="https://media.tenor.com/gUiu1zyxfzYAAAAi/bear-kiss-bear-kisses.gif" alt="gif" />
            <div className="text-container">
              Yajuuuuuuu muchimas gracias, nos vemos este miercoles para cenar!!!
            </div>
          </>
        ) : (
          <>
            <img
              className="h-[200px]"
              style={{ width: "400x", height: "240px" }}
              src="https://gifdb.com/images/high/cute-love-bear-roses-ou7zho5oosxnpo6k.gif"
              alt="gif"
            />
            <h1 className="text-container">¿Quieres ser mi valentin?</h1>
            <div>
              <button
                className={"yes-button"}
                style={{
                  fontSize: yesButtonSize,
                }}
                onClick={() => setYesPressed(true)}
              >
                Si
              </button>

              <button
                onMouseEnter={ handleNoMouseMove }
                onClick={ handleNoClick }
                className="no-button"
                style={mouseEnter ? {
                  position: "absolute",
                  left: noButtonPosition.x,
                  top: noButtonPosition.y,
                } : {}}
              >
                {noCount === 0 ? "No" : getNoButtonText()}
              </button>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
